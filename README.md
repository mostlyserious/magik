# Magik

A Django App containing reusable project utilities

## Installation

Both pip and pipenv support installing dependencies from git. Reference the documentation of the respective package manager being used to find out how to install.

## Documentation

### Database Utils

- Add `magik.database_utils` to the `INSTALLED_APPS` setting.
- Ensure these settings are set
``` python
AWS_ACCESS_KEY_ID = 'access-key'
AWS_SECRET_ACCESS_KEY = 'secret-access-lkey'
AWS_DB_DUMP_BUCKET_NAME = 'magik-db-dump-backups'
# Folder the DB dumps should be put in
AWS_DB_DUMP_BUCKET_PREFIX = 'magik-test'
```
- A `backup_db` command is now added and can be triggered via a cron script

### Template Utils

- Add `magik.template_utils` to the `INSTALLED_APPS` setting.

#### Mix
The mix template tag is used for automatic cache busting by referencing a `mix-manifest.json` file that contains file names and their hashes in a query string parameter.

Usage
```html
{% load mix %}
<link rel="stylesheet" type="text/css" href="{% mix 'css/main.css' %}">
<script src="{% mix 'js/index.js' %}"></script>
```

By default this tag looks for a `mix-manifest.json` file in the static folder, but this can be overwritten by adding a `MANIFEST_LOCATION` setting.

The final output of the mix tag will be the `STATIC_URL` setting + the url of the file with a cache busting query param taken from the `mix-manifest.json` file.

Note: if the url given to the mix tag is not in the `mix-manifest.json` file the mix tag will just return the `STATIC_URL` + the string passed to the mix tag

### Form Utils

#### Recaptcha

- Make the form class your trying to add Recaptcha to inherit from `magik.form_utils.forms.RecaptchaForm`
- Ensure that `RECAPTCHA_PRIVATE_KEY` is set

The form class that inherited from `RecaptchaForm` should now have a `verify_recaptcha` method that accepts the current `request`

### Storage Utils

- Add `magik.storage_utils` to `INSTALLED_APPS`

#### S3StaticFilesStorage
With django storages you can have the `collectstatic` command auto upload the collected static assets to an S3 bucket. By default this just uses the same bucket set in settings. This class adds the ability to change that bucket for static assets.

- Set `STATIC_FILES_STORAGE` to `magik.storage_utils.s3Boto.S3StaticFilesStorage`
- Set `AWS_STORAGE_STATIC_BUCKET_NAME` to be the bucket that static assets should go to
- Set `AWS_S3_CUSTOM_STATIC_DOMAIN` if using cloudfront to be the domain that static assets are served from

Note: These settings should probably only be set when the environment is in production mode. Usually we don't want these assets uploaded to S3 while in development
