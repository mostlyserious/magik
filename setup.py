from setuptools import setup, find_packages

setup(
    name='Magik',
    version='0.9.0',
    description='A Django App containing reusable project utilities',
    packages=find_packages(),
    install_requires=[
        'requests',
        'boto',
        'boto3'
    ],
)
