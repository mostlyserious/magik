import os
import re
import sys
import traceback
import subprocess
from datetime import datetime

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand, CommandError

import boto
from boto.s3.key import Key


#  ERROR_EMAILS = ('devops@mostlyserious.io')
ERROR_EMAILS = [x[1] for x in settings.MANAGERS]
ERROR_EMAIL_FROM = 'devops@mostlyserious.io'

DB_USER = settings.DATABASES['default']['USER']
DB_NAME = settings.DATABASES['default']['NAME']
BACKUP_PATH = r'.'
FILENAME_PREFIX = '%s.backup' % DB_NAME


def mail_message(text, subject='DB Backup Error', is_error=True):
    mail = EmailMultiAlternatives(
        subject=subject,
        body=text,
        from_email='Mostly Serious Devops <{}>'.format(ERROR_EMAIL_FROM),
        to=ERROR_EMAILS,
        headers={'Reply-To': ERROR_EMAIL_FROM}
    )
    success = mail.send()

    if is_error:
        sys.stderr.write('There was an error backing up the database.\n')
        sys.stderr.write(text)

    if success:
        print('Sent email.\n')


def catch_error(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as error:
            tb = traceback.format_exc()
            mail_message(tb)
            sys.exit()

    return func_wrapper


class Command(BaseCommand):
    help = 'Backs up this project\'s postgresql database'

    @catch_error
    def upload_backup(self, local_backup, filename):
        """
        Upload a file to an AWS S3 bucket.
        """
        s3 = S3Connection()

        s3.upload(local_backup, 'daily/' + filename)

        daily_backups = list(s3.list_files('daily/'))

        # Uploads backup to a weekly folder on Monday
        if datetime.today().weekday() == 0:
            oldest_daily = self.get_oldest_file_from_list(daily_backups)

            oldest_daily_filename = os.path.basename(oldest_daily.key)
            # s3.upload(local_backup, 'weekly/' + filename)
            s3.copy('daily/' + oldest_daily_filename, 'weekly/' + oldest_daily_filename)

            weekly_backups = list(s3.list_files('weekly/'))

            # Delete backups older than a month
            if len(weekly_backups) > 4:
                oldest_file = self.get_oldest_file_from_list(weekly_backups)
                s3.delete(oldest_file.key)

                print('Deleted', oldest_file.key)

        # Delete backups older than a week
        if len(daily_backups) > 7:
            oldest_file = self.get_oldest_file_from_list(daily_backups)
            s3.delete(oldest_file.key)

            print('Deleted', oldest_file.key)
    
    def get_datetime_from_file(self, file):
        """
        Takes the full path of a file as a string, and returns a datetime object.
        The date in the filename should be the last item in the filename

        Example input: 'magik-test/daily/magiktest.backup.2018-04-02.sql'
        """
        # Strip folder paths
        filename = os.path.basename(file)
        # Strip file extension
        filename_without_extension = os.path.splitext(filename)[0]
        # Split on period and grab last element which should be date
        datestring = filename_without_extension.split('.')[-1]

        return datetime.strptime(datestring, '%Y-%m-%d')

    
    def get_oldest_file_from_list(self, file_list):
        """
        Takes a list of S3 file objects and returns the one
        with the oldest date in its filename
        """

        dates = []

        for file in file_list:
            dates.append(self.get_datetime_from_file(file.key))
        
        oldest = min(dates)
        oldest_index = dates.index(oldest)

        return file_list[oldest_index]


    def handle(self, *args, **options):
        now = datetime.now()

        filename = '{}.{}-{:02d}-{:02d}.sql'.format(FILENAME_PREFIX, now.year, now.month, now.day)
        local_file = '{}/{}'.format(BACKUP_PATH, filename)

        self.stdout.write('%s Backing up %s database to %s' % (now, DB_NAME, local_file))

        # Dump database to local file
        ps = subprocess.Popen(
            ['pg_dump', '-U', DB_USER, '-w', '-Fp', DB_NAME,
                '-f', local_file, '--no-owner'],
            stdout=subprocess.PIPE
        )

        output = ps.communicate()[0]

        self.stdout.write('Uploading %s to Amazon S3...' % filename)

        self.upload_backup(local_file, filename)

        ps = subprocess.Popen(['rm', local_file], stdout=subprocess.PIPE)


class S3Connection:

    def __init__(self):
        self.bucket_prefix = settings.AWS_DB_DUMP_BUCKET_PREFIX + '/'

        self.conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,
                                    settings.AWS_SECRET_ACCESS_KEY)

        self.bucket = self.conn.get_bucket(settings.AWS_DB_DUMP_BUCKET_NAME)

    def upload(self, source, destination):
        obj = Key(self.bucket)
        obj.key = self.bucket_prefix + destination
        obj.set_contents_from_filename(source)
        obj.set_acl('private')
        return obj

    def delete(self, obj_key):
        obj = Key(self.bucket)
        obj.key = obj_key
        self.bucket.delete_key(obj)

    def copy(self, original_key, new_key):
        # print(self.bucket)
        new_key = self.bucket_prefix + new_key
        original_key = self.bucket_prefix + original_key
        self.bucket.copy_key(new_key, self.bucket.name, original_key)

    def list_files(self, prefix=''):
        for key in self.bucket.list(prefix=self.bucket_prefix + prefix):
            yield key
