from django.conf import settings
import requests


class RecaptchaForm:

    def verify_recaptcha(self, request):
        if 'g-recaptcha-response' not in request.POST:
            return False

        captcha_verify_data = {'secret': settings.RECAPTCHA_PRIVATE_KEY,
                               'response': request.POST['g-recaptcha-response']}
        captcha_verified = requests.post('https://www.google.com/recaptcha/api/siteverify',
                                         data=captcha_verify_data).json()

        if not captcha_verified['success']:
            return False

        return True
