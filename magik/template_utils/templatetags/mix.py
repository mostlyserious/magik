from django import template
from django.conf import settings
from urllib.parse import urljoin
import json

register = template.Library()


@register.simple_tag
def mix(path, manifest_directory='static'):

    if hasattr(settings, 'MANIFEST_LOCATION'):
        manifest_directory = settings.MANIFEST_LOCATION

    if not path.startswith('/'):
        path = '/{}'.format(path)

    if settings.DEBUG:
        try:
            f = open('{}/hot'.format(manifest_directory), 'r')
            url = f.readline()
            if url.startswith('https://') or url.startswith('http://'):
                url = url.split(':', 1)[1]
            return '{}{}'.format(url, path.strip('/'))
        except IOError:
            pass

    try:
        manifest_file = open('{}/mix-manifest.json'.format(manifest_directory), 'r').read()
        manifest = json.loads(manifest_file)
        return urljoin(settings.STATIC_URL, manifest.get(path, path).strip('/'))
    except IOError:
        return path
