from django.utils.safestring import mark_safe
from django.conf import settings
from django import template
import json
import os

register = template.Library()


@register.simple_tag
def entry(entry, manifest_path='static/assets/entries.json'):
    results = []

    if hasattr(settings, 'PROJECT_DIR'):
        manifest_path = os.path.join(settings.PROJECT_DIR, manifest_path)

    try:
        manifest_file = open(manifest_path, 'r').read()
        manifest = json.loads(manifest_file)
        entries = manifest.get(entry)

        for value in entries:
            result = False
            filename = ''
            ext = ''

            if isinstance(value, str):
                filename, ext = os.path.splitext(value)

            if ext == '.js':
                result = '<script src="{}" async defer></script>'.format(value)

            if ext == '.css':
                result = '<link href="{}" rel="stylesheet">'.format(value)

            if (result):
                results.append(result)

        return mark_safe("\n".join(results))
    except IOError:
        return 'Error getting "{}" entry.'.format(entry)


@register.simple_tag
def asset(entry, manifest_path='static/assets/assets.json'):
    results = []

    if hasattr(settings, 'PROJECT_DIR'):
        manifest_path = os.path.join(settings.PROJECT_DIR, manifest_path)

    try:
        manifest_file = open(manifest_path, 'r').read()
        manifest = json.loads(manifest_file)

        return manifest.get(entry)
    except IOError:
        return 'Error getting "{}" asset.'.format(entry)
