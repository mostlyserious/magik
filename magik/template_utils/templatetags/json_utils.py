import json

from django import template
from django.core.serializers import serialize

register = template.Library()


@register.filter()
def json_string(value):
    if isinstance(value, dict):
        return json.dumps(value)
    elif isinstance(value, str):
        return value
    else:
        return serialize('json', [value, ])
