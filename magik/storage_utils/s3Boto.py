from django.conf import settings
from storages.backends.s3boto import S3BotoStorage


class S3StaticFilesStorage(S3BotoStorage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket'] = settings.AWS_STORAGE_STATIC_BUCKET_NAME

        super().__init__(*args, **kwargs)

        if hasattr(settings, 'AWS_S3_CUSTOM_STATIC_DOMAIN'):
            self.custom_domain = settings.AWS_S3_CUSTOM_STATIC_DOMAIN
