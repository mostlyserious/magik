from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage


class S3StaticFilesStorage(S3Boto3Storage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket_name'] = settings.AWS_STORAGE_STATIC_BUCKET_NAME

        super().__init__(*args, **kwargs)

        if hasattr(settings, 'AWS_S3_CUSTOM_STATIC_DOMAIN'):
            self.custom_domain = settings.AWS_S3_CUSTOM_STATIC_DOMAIN

        if hasattr(settings, 'AWS_STATIC_ACCESS_KEY_ID'):
            self.access_key = settings.AWS_STATIC_ACCESS_KEY_ID

        if hasattr(settings, 'AWS_STATIC_SECRET_ACCESS_KEY'):
            self.secret_key = settings.AWS_STATIC_SECRET_ACCESS_KEY

        if hasattr(settings, 'AWS_STATIC_LOCATION'):
            self.location = settings.AWS_STATIC_LOCATION
